package com.safi.testapp.ui.post.addpost

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.safi.testapp.shared.data.pojo.Post
import com.safi.testapp.shared.data.repository.PostRepository
import com.safi.testapp.shared.ext.logD
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class AddPostViewModel(
    private val repository: PostRepository
) : ViewModel() {
    val title = MutableLiveData("")
    val body = MutableLiveData("")

    private val _event = MutableSharedFlow<Event>()
    val event: SharedFlow<Event> = _event

    fun onSaveClicked() = viewModelScope.launch {
        if (title.value.isNullOrEmpty()) {
            _event.emit(Event.TitleError("Title is required."))
            return@launch
        }
        if (body.value.isNullOrEmpty()) {
            _event.emit(Event.BodyError("Body is required."))
            return@launch
        }
        try {
            repository.addPost(Post(0, title.value!!, body.value!!)).let {
                it?.let {
                    _event.emit(Event.Success(it))
                }
            }
        } catch (e: Exception) {
            e.logD()
        }
    }

    sealed class Event {
        object Empty : Event()
        data class Success(val post: Post) : Event()
        data class TitleError(val error: String) : Event()
        data class BodyError(val error: String) : Event()
    }
}