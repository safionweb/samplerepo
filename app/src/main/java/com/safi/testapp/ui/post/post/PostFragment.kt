package com.safi.testapp.ui.post.post

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.safi.testapp.BR
import com.safi.testapp.R
import com.safi.testapp.databinding.FragmentPostBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostFragment : Fragment(R.layout.fragment_post) {
    private val viewModel: PostViewModel by viewModel()
    private var binding: FragmentPostBinding? = null
    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.bind(view)
        PostFragmentArgs.fromBundle(requireArguments()).post.let {
            viewModel.onItemReceived(it)
        }
        observePost()
    }

    private fun observePost() {
        viewModel.post.observe(viewLifecycleOwner) {
            binding!!.setVariable(BR.post, it)
        }
    }
}