package com.safi.testapp.ui.post.addpost

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.safi.testapp.R
import com.safi.testapp.databinding.FragmentAddPostBinding
import com.safi.testapp.shared.data.pojo.Post
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddPostFragment : Fragment(R.layout.fragment_add_post) {
    private val viewModel: AddPostViewModel by viewModel()
    private var binding: FragmentAddPostBinding? = null
    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FragmentAddPostBinding.bind(view).apply {
            binding = this
            viewModel = this@AddPostFragment.viewModel
            lifecycleOwner = this@AddPostFragment.viewLifecycleOwner
        }
        observeEvent()
    }

    private fun observeEvent() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.event.collect { event ->
                binding!!.apply {
                    when (event) {
                        AddPostViewModel.Event.Empty -> {}
                        is AddPostViewModel.Event.Success -> {
                            showSuccessDialog(event.post)
                        }
                        is AddPostViewModel.Event.BodyError ->
                            editTextBody.error = event.error
                        is AddPostViewModel.Event.TitleError ->
                            editTextTitle.error = event.error
                    }
                }
            }
        }
    }

    private fun showSuccessDialog(post: Post) {
        MaterialAlertDialogBuilder(requireActivity())
            .setTitle("Success")
            .setMessage("Post created successfully with the id: ${post.id}")
            .setPositiveButton("OK") { _, _ ->
                requireActivity().onBackPressed()
            }.show()
    }
}