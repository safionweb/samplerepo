package com.safi.testapp.ui.post.postlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.safi.testapp.shared.data.pojo.Post
import com.safi.testapp.shared.data.repository.PostRepository
import com.safi.testapp.shared.ext.logD
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class PostListViewModel(private val postRepository: PostRepository) : ViewModel() {
    private val _posts = MutableStateFlow<List<Post>>(listOf())
    val posts: StateFlow<List<Post>> = _posts

    init {
        reloadData()
    }

    private fun reloadData() = viewModelScope.launch {
        try {
            postRepository.getPosts().let {
                _posts.emit(it)
            }
        } catch (e: Exception) {
            e.logD()
        }
    }
}