package com.safi.testapp.ui.post.postlist

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import com.safi.testapp.BR
import com.safi.testapp.R
import com.safi.testapp.databinding.FragmentPostListBinding
import com.safi.testapp.databinding.ItemPostBinding
import com.safi.testapp.shared.data.pojo.Post
import com.safi.testapp.ui.adapter.BindingAdapter
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostListFragment : Fragment(R.layout.fragment_post_list) {
    private val viewModel: PostListViewModel by viewModel()
    private var binding: FragmentPostListBinding? = null
    private val adapter by lazy {
        BindingAdapter<Post, ItemPostBinding>(
            R.layout.item_post,
            BR.post,
            comparator
        ) { item, _ ->
            navigateToPostFragment(item)
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FragmentPostListBinding.bind(view).also {
            binding = it
            setupRecyclerView()
            it.buttonAdd.setOnClickListener {
                navigateToAddPostFragment()
            }
        }
    }


    private fun setupRecyclerView() {
        binding!!.recyclerView.adapter = adapter
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.posts.collect {
                adapter.submitList(it)
            }
        }
    }

    private fun navigateToPostFragment(post: Post) {
        PostListFragmentDirections.actionPostListFragmentToPostFragment(post).let {
            findNavController().navigate(it)
        }
    }

    private fun navigateToAddPostFragment() {
        PostListFragmentDirections.actionPostListFragmentToAddPostFragment().let {
            findNavController().navigate(it)
        }
    }

    private val comparator = object : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem == newItem

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem == newItem
    }
}