package com.safi.testapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class BindingAdapter<T, K : ViewDataBinding>(
    private val layoutId: Int,
    private val variableId: Int,
    comparator: DiffUtil.ItemCallback<T>,
    private val onItemClickListener: ((item: T, index: Int) -> Unit)? = null
) :
    ListAdapter<T, BindingAdapter.BindingViewHolder<K>>(comparator) {


    class BindingViewHolder<K>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: K? = DataBindingUtil.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<K> =
        BindingViewHolder(LayoutInflater.from(parent.context).inflate(layoutId, parent, false))

    override fun onBindViewHolder(holder: BindingViewHolder<K>, position: Int) {
        holder.binding?.apply {
            val item = getItem(position)
            setVariable(variableId, item)
            onItemClickListener?.let {
                root.setOnClickListener { it(item, position) }
            }
        }
    }

}