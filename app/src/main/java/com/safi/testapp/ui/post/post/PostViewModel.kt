package com.safi.testapp.ui.post.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.safi.testapp.shared.data.pojo.Post
import com.safi.testapp.shared.data.repository.PostRepository
import com.safi.testapp.shared.ext.logD
import kotlinx.coroutines.launch

class PostViewModel(
    private val postRepository: PostRepository
) : ViewModel() {
    private val _post = MutableLiveData<Post>()
    val post: LiveData<Post> = _post
    fun onItemReceived(post: Post) = viewModelScope.launch {
        try {
            postRepository.getPost(post.id).let {
                _post.postValue(it)
            }
        } catch (e: Exception) {
            e.logD()
        }
    }
}