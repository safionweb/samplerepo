package com.safi.testapp

import android.app.Application
import com.safi.testapp.di.appModule
import com.safi.testapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TestApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TestApp)
            modules(appModule)
            modules(viewModelModule)
        }
    }
}