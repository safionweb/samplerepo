package com.safi.testapp.di

import com.safi.testapp.ui.post.addpost.AddPostViewModel
import com.safi.testapp.ui.post.post.PostViewModel
import com.safi.testapp.ui.post.postlist.PostListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        PostListViewModel(get())
    }
    viewModel {
        PostViewModel(get())
    }
    viewModel {
        AddPostViewModel(get())
    }
}