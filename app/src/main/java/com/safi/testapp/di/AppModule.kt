package com.safi.testapp.di

import com.safi.testapp.shared.data.api.PostApi
import com.safi.testapp.shared.data.repository.PostRepository
import com.safi.testapp.shared.data.repository.PostRepositoryImpl
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {
    single {
        OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS).build();
    }
    single {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://graphqlzero.almansi.me/")
            .build()
    }
    single<PostApi> {
        get<Retrofit>().create(PostApi::class.java)
    }
    single<PostRepository> { PostRepositoryImpl(get()) }
}