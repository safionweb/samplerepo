package com.safi.testapp.shared.data.mutation

import com.safi.testapp.shared.data.pojo.Post
import com.safi.testapp.shared.data.query.GraphQLQuery

object PostMutationHelper {
    private val query = """
    mutation(${'$'}input:CreatePostInput!){createPost(input:${'$'}input){id, title, body}}
    """.trimIndent()

    fun createPost(post: Post) = GraphQLQuery(query, Variables(Input(post.title, post.body)))

    data class Variables(val input: Input)
    data class Input(val title: String, val body: String)

    data class CreatePostResponse(val createPost: Post)
}
