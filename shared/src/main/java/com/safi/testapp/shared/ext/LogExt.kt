package com.safi.testapp.shared.ext

import android.util.Log

private const val DEFAULT_TAG = "MyTag"
fun Any?.logD(tag: String? = DEFAULT_TAG) {
    Log.d(tag, this.toString())
}