package com.safi.testapp.shared.data.response

data class BaseResponse<T>(
    val data: T?
)