package com.safi.testapp.shared.data.query.helper

import com.safi.testapp.shared.data.query.GraphQLQuery

object PostQueryHelper {
    private val query = """
            query($ id: ID!){post(id: $ id){id,title,body}}
        """.trimIndent()

    fun getQuery(id: Int) = GraphQLQuery(query, PostVariables(id))
    data class PostVariables(private val id: Int)
}