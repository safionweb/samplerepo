package com.safi.testapp.shared.data.pojo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Post(
    val id: Int,
    val title: String,
    val body: String
) : Parcelable

data class PostResponse(val post: Post)
data class PostsResponse(val posts: PostsResult)
data class PostsResult(val data: List<Post>)