package com.safi.testapp.shared.data.query

data class GraphQLQuery(
    private val query: String,
    private val variables: Any?
)