package com.safi.testapp.shared.ext

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Response
import java.lang.reflect.Type

private val type: Type = object : TypeToken<ErrorResponse>() {}.type

fun <T> Response<T>.getResponseOrThrow(): T {
    if (isSuccessful)
        return body() ?: throw Exception("Body is null")
    val errorResponse = Gson().fromJson<ErrorResponse>(errorBody()?.charStream(), type)
    throw Exception(errorResponse.errors.map { it.message }.toString())
}

data class ErrorResponse(val errors: List<Error>) {
    data class Error(val message: String)
}