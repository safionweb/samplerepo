package com.safi.testapp.shared.ext

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Any.shortToast(context: Context) {
    Toast.makeText(context, this.toString(), Toast.LENGTH_SHORT).show()
}

fun Any.shortToast(fragment: Fragment) {
    Toast.makeText(fragment.context, this.toString(), Toast.LENGTH_SHORT).show()
}