package com.safi.testapp.shared.data.repository

import com.safi.testapp.shared.data.api.PostApi
import com.safi.testapp.shared.data.mutation.PostMutationHelper
import com.safi.testapp.shared.data.pojo.Post
import com.safi.testapp.shared.data.query.helper.PostQueryHelper
import com.safi.testapp.shared.data.query.helper.PostsQueryHelper
import com.safi.testapp.shared.ext.getResponseOrThrow

class PostRepositoryImpl(
    private val api: PostApi
) : PostRepository {
    override suspend fun getPost(id: Int): Post? =
        api.getPost(PostQueryHelper.getQuery(id)).getResponseOrThrow().data?.post

    override suspend fun getPosts(): List<Post> =
        api.getPosts(PostsQueryHelper.getQuery()).getResponseOrThrow().data?.posts?.data
            ?: listOf()


    override suspend fun addPost(post: Post): Post? =
        api.addPost(PostMutationHelper.createPost(post)).getResponseOrThrow().data?.createPost
}
