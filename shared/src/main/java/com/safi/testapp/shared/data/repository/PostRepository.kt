package com.safi.testapp.shared.data.repository

import com.safi.testapp.shared.data.pojo.Post

interface PostRepository {
    suspend fun getPost(id: Int): Post?
    suspend fun getPosts(): List<Post>
    suspend fun addPost(post: Post): Post?
}