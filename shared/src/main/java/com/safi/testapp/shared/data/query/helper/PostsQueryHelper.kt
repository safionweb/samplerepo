package com.safi.testapp.shared.data.query.helper

import com.safi.testapp.shared.data.query.GraphQLQuery

object PostsQueryHelper {
    private val query = """
            {posts{data{id,title}}}
        """.trimIndent()

    fun getQuery() = GraphQLQuery(query, null)
}