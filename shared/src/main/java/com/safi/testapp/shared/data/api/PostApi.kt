package com.safi.testapp.shared.data.api

import com.safi.testapp.shared.data.mutation.PostMutationHelper
import com.safi.testapp.shared.data.pojo.PostResponse
import com.safi.testapp.shared.data.pojo.PostsResponse
import com.safi.testapp.shared.data.query.GraphQLQuery
import com.safi.testapp.shared.data.response.BaseResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PostApi {
    @POST("api")
    suspend fun getPost(@Body query: GraphQLQuery): Response<BaseResponse<PostResponse>>

    @POST("api")
    suspend fun getPosts(@Body query: GraphQLQuery): Response<BaseResponse<PostsResponse>>

    @POST("api")
    suspend fun addPost(@Body query: GraphQLQuery): Response<BaseResponse<PostMutationHelper.CreatePostResponse>>
}